package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastroPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtGenero;
	private JTextField txtRG;
	private JTextField txtCPF;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastroPassageiro frame = new JFCadastroPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastroPassageiro() {
		setTitle("SisRodovi\u00E1ria - Cadastrar Passageiro");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 645, 372);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cadastrar Passageiro");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 11, 156, 17);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nome:");
		lblNewLabel_1.setBounds(10, 39, 38, 14);
		contentPane.add(lblNewLabel_1);
		
		txtNome = new JTextField();
		txtNome.setBounds(80, 39, 539, 17);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("G\u00EAnero:");
		lblNewLabel_2.setBounds(10, 64, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(80, 64, 539, 17);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("RG:");
		lblNewLabel_3.setBounds(10, 89, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		txtRG = new JTextField();
		txtRG.setBounds(80, 89, 539, 17);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("CPF:");
		lblNewLabel_4.setBounds(10, 114, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(80, 114, 539, 17);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("Endere\u00E7o:");
		lblNewLabel_5.setBounds(10, 139, 60, 14);
		contentPane.add(lblNewLabel_5);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(80, 139, 539, 17);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("E-mail:");
		lblNewLabel_6.setBounds(10, 164, 46, 14);
		contentPane.add(lblNewLabel_6);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(80, 164, 539, 17);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Telefone:");
		lblNewLabel_7.setBounds(10, 189, 60, 14);
		contentPane.add(lblNewLabel_7);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(80, 189, 539, 17);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Passageiro f = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				f.setNome(txtNome.getText());
				f.setGenero(txtGenero.getText());
				f.setRg(Long.parseLong(txtRG.getText()));
				f.setCpf(Long.parseLong(txtCPF.getText()));
				f.setEnderešo(txtEndereco.getText());
				f.setEmail(txtEmail.getText());
				f.setTelefone(Long.parseLong(txtTelefone.getText()));
				
				dao.create(f);
				
			}
		});
		btnCadastrar.setBounds(99, 263, 108, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setBounds(256, 263, 108, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(412, 263, 108, 23);
		contentPane.add(btnCancelar);
	}
}
