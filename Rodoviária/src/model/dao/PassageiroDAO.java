package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {

	public void create(Passageiro f) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (nome, genero, rg, cpf, endere�o, email, telefone) VALUES (?,?,?,?,?,?,?)");
			stmt.setString(1, f.getNome());
			stmt.setString(2, f.getGenero());
			stmt.setLong(3, f.getRg());
			stmt.setLong(4, f.getCpf());
			stmt.setString(5, f.getEndere�o());
			stmt.setString(6, f.getEmail());
			stmt.setLong(7, f.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro f = new Passageiro();
				f.setIdPassageiro(rs.getInt("idpassageiro"));
				f.setNome(rs.getString("nome"));
				f.setGenero(rs.getString("genero"));
				f.setRg(rs.getLong("rg"));
				f.setCpf(rs.getLong("cpf"));
				f.setEndere�o(rs.getString("endere�o"));
				f.setEmail(rs.getString("email"));
				f.setTelefone(rs.getLong("telefone"));
				passageiros.add(f);
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}
	
	public void delete(Passageiro f) {
		
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE idPassageiro=?");
			stmt.setInt(1, f.getIdPassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passageiro exclu�do com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
		}finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
		
	}
	
	public Passageiro read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Passageiro f = new Passageiro();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE idPassageiro=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if(rs != null && rs.next()) {
				f.setIdPassageiro(rs.getInt("idpassageiro"));
				f.setNome(rs.getString("nome"));
				f.setGenero(rs.getString("genero"));
				f.setRg(rs.getLong("rg"));
				f.setCpf(rs.getLong("cpf"));
				f.setEndere�o(rs.getString("endere�o"));
				f.setEmail(rs.getString("email"));
				f.setTelefone(rs.getLong("telefone"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return f;
	}
	
	public void update(Passageiro f) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("UPDATE passageiro SET nome=?, genero=?, rg=?, cpf=?, endere�o=?, email=?, telefone=? WHERE idPassageiro=?;");
			stmt.setString(1, f.getNome());
			stmt.setString(2, f.getGenero());
			stmt.setLong(3, f.getRg());
			stmt.setLong(4, f.getCpf());
			stmt.setString(5, f.getEndere�o());
			stmt.setString(6, f.getEmail());
			stmt.setLong(7, f.getTelefone());
			stmt.setInt(8, f.getIdPassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
}
